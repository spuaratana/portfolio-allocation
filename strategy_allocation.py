import numpy as np
import sys
from cvxpy import *
import matplotlib.pyplot as plt

# Generate 10 random strategy pnl series
np.random.seed(1)
n_day = 3*256
n_strategy = 10

mu_list = [10,9,8,7,6,5,4,3,2,1]
sigma_list = [30,20,10,35,40,5,15,20,25,5]
strategy_pnl_matrix = np.zeros(shape=[n_strategy,n_day])
for j in range(n_strategy):
    s = np.random.normal(mu_list[j], sigma_list[j], n_day)
    strategy_pnl_matrix[j,:] = s

# Calculate Stats from optimisation
mu = np.nanmean(strategy_pnl_matrix,axis=1)
Cov = np.cov(strategy_pnl_matrix)
inv_diag = 1/np.sqrt(np.diag(Cov))
Cor = inv_diag*Cov*inv_diag
shrink_factor = 0.25
Sigma = (1.0-shrink_factor)*Cov + shrink_factor*np.nanmean(np.diag(Cov))*np.eye(n_strategy)

# Penalty Rate Calculation
normsize = np.array([5,6,7,3,7,5,4,3,4,3])
normsize_ret = np.array([10,9,8,7,6,5,4,3,2,1])
maxsize = np.array([9,9,10,30,10,6,20,7,10,10])
maxsize_ret = np.array([4,7,6,5,3,4,2,1,1,0])
ret_penalty_rate = 1.0*(normsize_ret-maxsize_ret)/(maxsize-normsize)
a = np.log(1.0 + ret_penalty_rate*(maxsize-normsize)*maxsize)/(maxsize-normsize)
min_portfolio_size = 40
max_portfolio_size = 40

# Long only portfolio optimization.
w = Variable(n_strategy)
c = Parameter(nonneg=True)
ret = mu.T*w
pos_w = pos(w-normsize)
individual_ret_penalty = exp(multiply(a,pos(w-normsize)))-1
ret_penalty = sum(individual_ret_penalty)
dollar_return = ret - ret_penalty
risk = quad_form(w, Sigma)
prob = Problem(Maximize(ret - ret_penalty - c*risk), [np.ones(shape=[n_strategy,])*w <= max_portfolio_size,np.ones(shape=[n_strategy,])*w >= min_portfolio_size, w >= 0])

# Compute trade-off curve.
SAMPLES = 1000
risk_data = np.zeros(SAMPLES)
ret_data = np.zeros(SAMPLES)
dollar_ret_data = np.zeros(SAMPLES)
total_traded_value_data = np.zeros(SAMPLES)
c_vals = np.logspace(-6, 2.5, num=SAMPLES)
w_matrix = np.zeros(shape=[SAMPLES,n_strategy])

not_found_solutions = 0
not_found_c_list = []
for i in range(SAMPLES):
    c.value = c_vals[i]
    prob.solve(solver=SCS,verbose=False)

    if w.value is None:
        not_found_c_list.append(c_vals[i])
        not_found_solutions = not_found_solutions + 1

    ret_data[i] = dollar_return.value/np.sum(w.value)
    risk_data[i] = sqrt(risk).value*1000000/10000
    dollar_ret_data[i] = dollar_return.value*1000000/10000
    total_traded_value_data[i] = np.sum(w.value)
    w_matrix[i,:] = w.value

print(not_found_solutions)
print(not_found_c_list)

plt.imshow(w_matrix, cmap='hot', aspect='auto')
plt.show()

plt.plot(risk_data,total_traded_value_data, 'ro')
plt.xlabel('Standard deviation')
plt.ylabel('Total trading size')
plt.show()

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.plot(risk_data, dollar_ret_data, 'g-')
ax2.plot(risk_data, np.log10(c_vals), 'b-')
ax1.set_xlabel('Standard deviation')
ax1.set_ylabel('Return', color='g')
ax2.set_ylabel('log(c)', color='b')
plt.show()

input_c = input("Please select your value of log(c)")
c.value = pow(10,float(input_c))
prob.solve(solver=SCS, verbose=False)
print(w.value)
print(dollar_return.value*1000000/10000)
print(sqrt(risk).value*1000000/10000)

